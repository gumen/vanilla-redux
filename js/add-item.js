import store from './store.js'
import actions from './actions.js'

class AddItem extends HTMLElement {
  constructor() {
    super()

    this.attachShadow({mode: 'open'})
    this.shadowRoot.innerHTML =`
      <form>
        <input type="text" />
        <input type="submit" value="add" />
      </form>
    `

    const elementForm  = this.shadowRoot.querySelector('form')
    const elementInput = this.shadowRoot.querySelector('[type="text"]')

    elementForm.addEventListener('submit', (event) => {
      event.preventDefault()
      elementInput.value = elementInput.value.trim()
      if(!elementInput.value) return // do nothing for empty value
      store.dispatch(actions.addItem(elementInput.value))
      elementInput.value = ''
    })
  }
}

customElements.define('add-item', AddItem)
