let store;
const eventSubscribe = new CustomEvent('store:subscribe')

export const getState = () => store

export const subscribe = (callback) =>
  document.addEventListener('store:subscribe', () => callback(getState()))

export const dispatch = (action) =>
  document.dispatchEvent(new CustomEvent('store:dispatch', { detail: action }))

export default { getState, subscribe, dispatch }

// reducer
const reducer = (state = [], action) => {
  switch (action.type) {
  case 'ADD_ITEM':
    return [...state, action.text]
  default:
    return state
  }
}

document.addEventListener('store:dispatch', (event) => {
  store = reducer(store, event.detail)
  document.dispatchEvent(eventSubscribe)
})

dispatch({}) // init store
