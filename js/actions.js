export default {
  addItem: (text) => ({ type: 'ADD_ITEM', text })
}
