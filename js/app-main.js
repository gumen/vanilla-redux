import store from './store.js'
import actions from './actions.js'

class AppMain extends HTMLElement {
  constructor() {
    super()

    this.attachShadow({mode: 'open'}) // Available throu this.shadowRoot

    store.subscribe((state) => this.render(state))
    store.dispatch(actions.addItem('new item'))
    store.dispatch(actions.addItem('another item'))
  }

  render(state) {
    const items = state.map(item => `<li>${item}</li>`).join('')
    this.shadowRoot.innerHTML = `<ul>${items}</ul>`
  }
}

customElements.define('app-main', AppMain)
