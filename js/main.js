import actions from './actions.js'
import './app-main.js'
import './add-item.js'
import store from './store.js'

store.dispatch(actions.addItem('item added from main'))
store.dispatch(actions.addItem('item 2 added from main'))

